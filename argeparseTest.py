import argparse

parser = argparse.ArgumentParser()
parser.add_argument("square",help = "display a square of a given number",
		    type = int)

parser.add_argument("-v","--verbose", help = "Increase output verbosity", 
		    action = "store_true")
args = parser.parse_args()
answer =  args.square**2
if args.verbose:
	print("the square of {} equails  ".format(args.square)+str(answer))

else:
	print(answer)


#Ce sont des tests